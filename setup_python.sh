# setup venv packages, but not full virtual environment

if [ -z "$ACTS_BUILD_DIR" ]; then
  ACTS_BUILD_DIR="$(readlink -f $(dirname $(readlink -f "$BASH_SOURCE"))/build)"
  export ACTS_BUILD_DIR
fi

PYTHONUNBUFFERED=1  # keep Python and C++ printouts in sync. Without this, some Python messages to non-terminal stdout will be held till the end of the program.
VIRTUAL_ENV="$ACTS_BUILD_DIR/venv"
XDG_CACHE_HOME="$VIRTUAL_ENV/cache"
PATH="$VIRTUAL_ENV/bin:$PATH"
PYTHONPATH="$VIRTUAL_ENV/lib/python3.9/site-packages:$VIRTUAL_ENV/lib/python3.7/site-packages:$PYTHONPATH"
export VIRTUAL_ENV XDG_CACHE_HOME PATH PYTHONPATH PYTHONUNBUFFERED
# unset PYTHONHOME  # this broke gdb, but maybe isn't needed any more for pytest?

[ -f "$ACTS_BUILD_DIR/python/setup.sh" ] && . "$ACTS_BUILD_DIR/python/setup.sh" > /dev/null
