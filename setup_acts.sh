if [ -n "$1" ]; then
  export ACTS_BUILD_DIR="$(readlink -f $1)"
else
  export ACTS_BUILD_DIR="$(readlink -f $(dirname $(readlink -f "$BASH_SOURCE"))/build)"
fi
if [ -d "$ACTS_BUILD_DIR/thirdparty/OpenDataDetector/factory" ]; then
  odd_path="$ACTS_BUILD_DIR/thirdparty/OpenDataDetector/factory:"
else
  odd_path=""
fi
#. "$ACTS_BUILD_DIR/this_acts.sh"
export PATH="$ACTS_BUILD_DIR/bin${PATH:+:}${PATH}"
export LD_LIBRARY_PATH="$odd_path$ACTS_BUILD_DIR/lib64${LD_LIBRARY_PATH:+:}${LD_LIBRARY_PATH}"
export DYLD_LIBRARY_PATH="$odd_path$ACTS_BUILD_DIR/lib64${DYLD_LIBRARY_PATH:+:}${DYLD_LIBRARY_PATH}"
unset odd_path
