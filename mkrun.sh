#!/bin/bash

ppd_readlink()
{
  readlink "$@" | sed 's=^/net/home/ppd/=/home/ppd/='
}

if [ $# -lt 1 ]; then
  echo "Usage: $(basename "$0") RUN-DIRNAME [BUILD-DIRNAME] [SRC-DIRNAME]" >&2
  echo "Eg. $(basename "$0") run04 build04" >&2
  exit 1
fi
sname="$1"
bname="$2"
src="$3"
[ -n "$bname" ] || bname="${sname/#run/build}"
[ -n "$src" ] || src="acts"

cwd=$(dirname $(ppd_readlink -f "$BASH_SOURCE"))
[ -d "$cwd/tmp" ] && notmp=0 || notmp=1
if [ $notmp -eq 0 ]; then
  if [ -L "$cwd/tmpdir" ]; then
    TMP=$(readlink -f "$cwd/tmpdir")/
  else
    test -z "$TMPDIR" && TMPDIR="/tmp/`id -un`"
    TMP="$TMPDIR/acts"
    ln -nfs "$TMP" "$cwd/tmpdir"
    TMP="$TMP/"
  fi
[ -z "${TMP%%/tmp/*}" -o -z "${TMP%%/scratch/*}" ] && suf=".$(hostname -s)" || suf=""
else
  TMP=""
  suf=""
fi
name="$sname$suf"

set -x
mkdir -p "$TMP$sname"
[ $notmp -eq 0 ] && ln -nfs "$TMP$sname" "$cwd/tmp/$name"
ln -nfs "$cwd" "$TMP$sname/acts"
ln -nfs "acts/setup.sh" "$TMP$sname/setup.sh"
ln -nfs "acts/$src" "$TMP$sname/src"
ln -nfs "../$bname" "$TMP$sname/build"
ln -nfs "acts/acts-itk" "$TMP$sname/acts-itk"
[ $notmp -eq 0 ] && ln -nfs "tmp/$name" "$cwd/$sname"
