#!/bin/bash
osrel=$(uname -r)
[ -z "${osrel%%*-WSL2}" ] && export PATH=${PATH%%:/mnt/*}    # hack for WSL2 with Windows paths with spaces in $PATH
if [ -f "/opt/lcg/clang/10.0.1-9c2fe/x86_64-centos7/setup.sh" ]; then
  . /opt/lcg/clang/10.0.1-9c2fe/x86_64-centos7/setup.sh
else
  . /cvmfs/sft.cern.ch/lcg/releases/clang/10.0.1-9c2fe/x86_64-centos7/setup.sh
fi
cd $(dirname "$0")/acts
CI/check_format .
