#!/usr/bin/env python3
# from pathlib import Path

# import acts
# import acts.examples

# u = acts.UnitConstants

import acts.examples
field = acts.examples.MagneticFieldMapXyz("acts-itk/bfield/ATLAS-BField-xyz.root")
cfg = acts.examples.CsvBFieldWriter.ConfigXyzGrid()
cfg.bField = field
cfg.fileName = "ATLAS-BField-xyz.csv"
acts.examples.CsvBFieldWriter.runXyzGrid(cfg, acts.logging.VERBOSE)



# def runBFieldWriting(outputDir: Path):
#     field = acts.examples.MagneticFieldMapXyz("acts-itk/bfield/ATLAS-BField-xyz.root")
#     print ("read", field)

#     cfg = acts.examples.CsvBFieldWriter.ConfigXyzGrid()
#     cfg.bField = field
#     # cfg.gridType = acts.examples.RootBFieldWriter.GridType.xyz
#     cfg.fileName = str(outputDir / "ATLAS-BField-xyz.csv")
#     # cfg.treeName = "bField"

#     print(f"Now write {cfg.fileName}")
#     # acts.examples.RootBFieldWriter.run(cfg, acts.logging.VERBOSE)
#     acts.examples.CsvBFieldWriter.runXyzGrid(cfg, acts.logging.VERBOSE)


# if "__main__" == __name__:
#     runBFieldWriting(Path.cwd())
