#!/usr/bin/env python3

# needed if this script is a symlink to another directory
import sys, os

sys.path.insert(0, os.path.dirname(__file__))

import pathlib, argparse, acts, acts.examples, seeding, acts.examples.reconstruction

p = argparse.ArgumentParser(
    description="Example script to run full chain ITk reconstruction",
)
p.add_argument(
    "-n",
    "--events",
    type=int,
    default=100,
    help="The number of events to process (default=100).",
)
p.add_argument(
    "-N",
    "--gen-nparticles",
    type=int,
    help="Number of generated particles per event (default=2) or number of pileup events (default=200).",
)
p.add_argument(
    "-j",
    "--jobs",
    type=int,
    default=-1,
    help="Number of parallel jobs, negative for automatic (default).",
)
p.add_argument(
    "-t",
    "--ttbar-pu200",
    action="store_true",
    help="Generate ttbar + mu=200 pile-up using Pythia8",
)
p.add_argument(
    "-l",
    "--loglevel",
    type=int,
    default=2,
    help="The output log level. Please set the wished number (0 = VERBOSE, 1 = DEBUG, 2 = INFO (default), 3 = WARNING, 4 = ERROR, 5 = FATAL).",
)
p.add_argument(
    "-p",
    "--gen-mom-gev",
    help="pT - transverse momentum generation range in GeV (default 1:10 or 1: with -t)",
)
p.add_argument(
    "-o",
    "--output-dir",
    default=None,
    type=pathlib.Path,
    help="Directory to write outputs to.",
)
p.add_argument(
    "-a",
    "--algorithm",
    action=seeding.EnumAction,
    enum=acts.examples.reconstruction.SeedingAlgorithm,
    default=acts.examples.reconstruction.SeedingAlgorithm.Default,
    help="Select the seeding algorithm to use",
)
p.add_argument(
    "-J",
    "--itk-tgeo-json-config",
    action="store_true",
    help="Configure ITk TGeo with JSON file",
)
p.add_argument(
    "-e",
    "--gen-cos-theta",
    action="store_true",
    help="Sample eta as cos(theta) and not uniform",
)
p.add_argument(
    "-b",
    "--bf-constant",
    action="store_true",
    help="Use constant 2T B-field; and don't include material map",
)
p.add_argument(
    "-g",
    "--generic-detector",
    action="store_true",
    help="Use generic detector geometry with ITk settings for quick test",
)
p.add_argument(
    "--odd",
    action="store_true",
    help="Use Open Data Detector geometry with ITk settings for quick test",
)
p.add_argument(
    "--geant4",
    action="store_true",
    help="Use Geant4 instead of Fatras for dectector simulation",
)
p.add_argument(
    "-d",
    "--dump-args-calls",
    action="store_true",
    help="Show pybind function call details",
)
p.add_argument(
    "-G",
    "--old-geometry",
    action="store_true",
    help="Use old ATLAS-P2-23 instead of new ATLAS-P2-RUN4-01-00-00",
)
p.add_argument(
    "-R",
    "--no-ParticleSelector",
    "--no-TruthSeedRanges",  # old name
    action="count",
    default=0,
    help="don't specify postSelectParticles on Fatras/Geant4 (default unless -t). Specify -RR to disable TruthSeedRanges altogether.",
)
p.add_argument(
    "-O",
    "--reduce-output",
    action="count",
    default=0,
    help="don't write intermediate results. Use -OO to disable all output.",
)
p.add_argument(
    "-F",
    "--disable-fpemon",
    action="store_true",
    help="sets ACTS_SEQUENCER_DISABLE_FPEMON=1",
)
p.add_argument(
    "-T",
    "--old-track-selector",
    action="count",
    default=0,
    help="old TrackSelector, without eta-dependent cuts from Athena. Specify -TT to disable TrackSelector altogether",
)
p.add_argument(
    "-B",
    "--no-two-way",
    action="store_true",
    help="Don't use two-way CKF.",
)
p.add_argument(
    "-C",
    "--no-ckf-opt",
    action="count",
    default=0,
    help="Don't use seed deduplication nor stick on the seed measurements during track finding. Specify -CC to only turn off additional CKF selections: max pixel/strip holes",
)
args = p.parse_args()

if args.disable_fpemon:
    os.environ["ACTS_SEQUENCER_DISABLE_FPEMON"] = "1"

if args.old_geometry:
    import acts.examples.itk_test as itk
else:
    import acts.examples.itk as itk

u = acts.UnitConstants
if args.gen_mom_gev is None:
    # didn't need to do this, because max pT isn't used for ttbar
    args.gen_mom_gev = "1:" if args.ttbar_pu200 else "1:10"
pt = [float(p) * u.GeV if p != "" else None for p in args.gen_mom_gev.split(":")]
if args.gen_nparticles is None:
    args.gen_nparticles = 200 if args.ttbar_pu200 else 2
if args.no_ParticleSelector == 0 and not args.ttbar_pu200:
    args.no_ParticleSelector = 1

if args.dump_args_calls:
    acts.examples.dump_args_calls(locals())

logger = acts.logging.getLogger("full_chain_itk_test")

if args.reduce_output >= 2:
    outputDirFinal = None
elif args.output_dir is None:
    outputDirFinal = pathlib.Path.cwd() / "itk_output"
else:
    outputDirFinal = args.output_dir
outputDir = None if args.reduce_output >= 1 else outputDirFinal


# fmt: off
if args.generic_detector:
    etaRange = (-2.0, 2.0)
    geo_dir = pathlib.Path(acts.__file__).resolve().parent.parent.parent.parent.parent
    digiConfigFile = geo_dir / "Examples/Algorithms/Digitization/share/default-smearing-config-generic.json"
    seedingConfigFile = geo_dir / "Examples/Algorithms/TrackFinding/share/geoSelection-genericDetector.json"
    materialMapFile = None
    bFieldFile = pathlib.Path("acts-itk") / "bfield/ATLAS-BField-xyz.root"  # no generic detector non-uniform B-field
    detector, trackingGeometry, decorators = acts.examples.GenericDetector.create()
elif args.odd:
    import acts.examples.odd

    etaRange = (-3.0, 3.0)
    geo_dir = acts.examples.odd.getOpenDataDetectorDirectory()
    digiConfigFile = geo_dir / "config/odd-digi-smearing-config.json"
    seedingConfigFile = geo_dir / "config/odd-seeding-config.json"
    materialMapFile = geo_dir / "data/odd-material-maps.root"
    bFieldFile = pathlib.Path("acts-itk") / "bfield/ATLAS-BField-xyz.root"  # no ODD non-uniform B-field
    detector, trackingGeometry, decorators = acts.examples.odd.getOpenDataDetector(
        odd_dir=geo_dir,
        mdecorator=acts.IMaterialDecorator.fromFile(materialMapFile)
        if not args.bf_constant
        else None,
    )
else:  # ITk
    etaRange = (-4.0, 4.0)
    geo_dir = pathlib.Path("acts-itk")
    digiConfigFile = geo_dir / "itk-hgtd/itk-smearing-config.json"
    seedingConfigFile = geo_dir / "itk-hgtd/geoSelection-ITk.json"
    materialMapFile = None  # defaulted in itk.buildITkGeometry: geo_dir / "itk-hgtd/material-maps-ITk-HGTD.json"
    bFieldFile = geo_dir / "bfield/ATLAS-BField-xyz.root"
    detector, trackingGeometry, decorators = itk.buildITkGeometry(
        geo_dir,
        customMaterialFile=materialMapFile,
        material=not args.bf_constant,
        jsonconfig=args.itk_tgeo_json_config,
        logLevel=acts.logging.Level(args.loglevel),
        **(
            dict(oldGeometry=True)  # only available in my itk_test.py
            if args.old_geometry
            else {}
        ),
    )
# fmt: on

if args.bf_constant:
    field = acts.ConstantBField(acts.Vector3(0.0, 0.0, 2.0 * u.T))
else:
    logger.info("Create magnetic field map from %s" % str(bFieldFile))
    field = acts.examples.MagneticFieldMapXyz(str(bFieldFile))
rnd = acts.examples.RandomNumbers(seed=42)


from acts.examples.simulation import (
    MomentumConfig,
    EtaConfig,
    ParticleConfig,
    ParticleSelectorConfig,
    addDigitization,
)
from acts.examples.reconstruction import (
    addSeeding,
    ParticleSmearingSigmas,
    addCKFTracks,
    CkfConfig,
    TrackSelectorConfig,
    addAmbiguityResolution,
    AmbiguityResolutionConfig,
    addVertexFitting,
    VertexFinder,
)

s = acts.examples.Sequencer(
    events=args.events,
    numThreads=args.jobs if not (args.geant4 and args.jobs == -1) else 1,
    logLevel=acts.logging.Level(args.loglevel),
    outputDir="" if outputDirFinal is None else str(outputDirFinal),
)

if not args.ttbar_pu200:
    from acts.examples.simulation import addParticleGun

    addParticleGun(
        s,
        MomentumConfig(*pt, transverse=True),
        EtaConfig(*etaRange, uniform=not args.gen_cos_theta),
        ParticleConfig(
            args.gen_nparticles, acts.PdgParticle.eMuon, randomizeCharge=True
        ),
        rnd=rnd,
        outputDirRoot=outputDir
        if args.output_dir is not None
        else None,  # disable output if -o not specified, to match full_chain_itk.py behaviour
    )
else:
    from acts.examples.simulation import addPythia8

    addPythia8(
        s,
        hardProcess=["Top:qqbar2ttbar=on"],
        npileup=args.gen_nparticles,
        vtxGen=acts.examples.GaussianVertexGenerator(
            stddev=acts.Vector4(0.0125 * u.mm, 0.0125 * u.mm, 55.5 * u.mm, 5.0 * u.ns),
            mean=acts.Vector4(0, 0, 0, 0),
        ),
        rnd=rnd,
        outputDirRoot=outputDir,
    )

preSelectParticles = (
    ParticleSelectorConfig(
        rho=(0.0 * u.mm, 28.0 * u.mm),
        absZ=(0.0 * u.mm, 1.0 * u.m),
        eta=etaRange,
        pt=(150 * u.MeV, None),
        removeNeutral=True,
    )
    if args.ttbar_pu200 or args.geant4
    else ParticleSelectorConfig()
)

postSelectParticles = (
    ParticleSelectorConfig(
        pt=(pt[0], None),
        eta=etaRange,
        measurements=(9, None),
        removeNeutral=True,
    )
    if args.no_ParticleSelector == 0
    else ParticleSelectorConfig()
    if args.no_ParticleSelector == 1
    else None
)

if not args.geant4:
    from acts.examples.simulation import addFatras

    addFatras(
        s,
        trackingGeometry,
        field,
        rnd=rnd,
        preSelectParticles=preSelectParticles,
        postSelectParticles=postSelectParticles,
        outputDirRoot=outputDir,
    )
else:
    if s.config.numThreads != 1:
        logger.error(f"****** Geant 4 simulation does not support multi-threading (threads={s.config.numThreads}) ******")

    from acts.examples.simulation import addGeant4

    # Pythia can sometime simulate particles outside the world volume, a cut on the Z of the track help mitigate this effect
    # Older version of G4 might not work, this as has been tested on version `geant4-11-00-patch-03`
    # For more detail see issue #1578
    addGeant4(
        s,
        detector,
        trackingGeometry,
        field,
        rnd=rnd,
        preSelectParticles=preSelectParticles,
        postSelectParticles=postSelectParticles,
        outputDirRoot=outputDir,
        killVolume=trackingGeometry.highestTrackingVolume,
        killAfterTime=25 * u.ns,
    )

addDigitization(
    s,
    trackingGeometry,
    field,
    digiConfigFile=digiConfigFile,
    outputDirRoot=outputDir,
    rnd=rnd,
)

addSeeding(
    s,
    trackingGeometry,
    field,
    *itk.itkSeedingAlgConfig(  # only needed for SeedingAlgorithm.Default
        itk.InputSpacePointsType.PixelSpacePoints,
    ),
    ParticleSmearingSigmas(ptRel=0.01),  # only needed for SeedingAlgorithm.TruthSmeared
    seedingAlgorithm=args.algorithm,
    rnd=rnd,  # only needed for SeedingAlgorithm.TruthSmeared
    initialSigmas=[
        1 * u.mm,
        1 * u.mm,
        1 * u.degree,
        1 * u.degree,
        0.1 * u.e / u.GeV,
        1 * u.ns,
    ],
    initialSigmaPtRel=0.1,
    initialVarInflation=[1.0] * 6,
    geoSelectionConfigFile=seedingConfigFile,
    outputDirRoot=outputDirFinal,
)

addCKFTracks(
    s,
    trackingGeometry,
    field,
    # fmt: off
    trackSelectorConfig=(
        TrackSelectorConfig(absEta=(None, 2.0), pt=(0.9 * u.GeV, None), nMeasurementsMin=9, maxHoles=2, maxOutliers=2, maxSharedHits=2),
        TrackSelectorConfig(absEta=(None, 2.6), pt=(0.4 * u.GeV, None), nMeasurementsMin=8, maxHoles=2, maxOutliers=2, maxSharedHits=2),
        TrackSelectorConfig(absEta=(None, 4.0), pt=(0.4 * u.GeV, None), nMeasurementsMin=7, maxHoles=2, maxOutliers=2, maxSharedHits=2),
    )
    if args.old_track_selector == 0
    else TrackSelectorConfig(pt=(pt[0] if args.ttbar_pu200 else 0.0, None), absEta=(None, etaRange[1]), nMeasurementsMin=6)
    if args.old_track_selector == 1
    else None,
    ckfConfig=CkfConfig(
        seedDeduplication=True,
        stayOnSeed=True,
        **(dict(
            # ITk volumes from Noemi's plot
            pixelVolumes=[8, 9, 10, 13, 14, 15, 16, 18, 19, 20],
            stripVolumes=[22, 23, 24],
            maxPixelHoles=1,
            maxStripHoles=2,
        ) if args.no_ckf_opt != 2 else {})
    ) if args.no_ckf_opt in (0,2) else CkfConfig(),
    **(dict(writeTrackSummary=False) if args.reduce_output >= 1 else {}),
    outputDirRoot=outputDirFinal,
    # fmt: on
    **(dict(twoWay=not args.no_two_way) if args.no_two_way else {}),
)

addAmbiguityResolution(
    s,
    AmbiguityResolutionConfig(
        maximumSharedHits=3,
        maximumIterations=10000,
        nMeasurementsMin=6,
    ),
    **(dict(writeTrackSummary=False) if args.reduce_output >= 1 else {}),
    outputDirRoot=outputDir,
)

addVertexFitting(
    s,
    field,
    vertexFinder=VertexFinder.AMVF,
    outputDirRoot=outputDir,
)

s.run()
