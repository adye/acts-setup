#!/bin/bash
# Copy listed RootHashAssertionErrors from pytest (run without custom ROOT_HASH_FILE) into c7.txt.
# Then run this script.
changes="$1"
[ -z "$changes" ] && changes="c7.txt"
dir=$(dirname "$0")
sed -f <(sed -E 's/([.]|\[|\])/\\\1/g;s=^(.*): (.*)$=s/^\1: .*$/\1: \2/=' c7.txt) $dir/root_file_hashes.txt > $dir/root_file_hashes_c7.txt
