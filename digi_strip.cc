// Simple toy MC to simulate ITk strip digitisation
// also try pixel digi, but that's not so interesting!

#include <cmath>

#include "TH1.h"
#include "TH2.h"
#include "TRandom.h"

TH2D* htru = 0;
TH2D* hrec = 0;
TH1D* xres = 0;
TH1D* yres = 0;
TH1D* xpul = 0;
TH1D* ypul = 0;

void digi_strip (
                 int nhit=1000000,
                 double upitch=0.0755,  // 75.5um pitch
                 double vpitch=0.0755,  // 75.5um pitch
                 double ustereo=0.026,  // u-strip +stereo radians from x-axis
                 double vstereo=-0.026, // v-strip -stereo radians from x-axis.
                 double xlo=0.0,
                 double xhi=100.0,
                 double ylo=0.0,
                 double yhi=100.0,
                 int nbx=10000,
                 int nby=1000,
                 int nb=500
                ) {

  const double xwid = xhi-xlo;
  const double ywid = yhi-ylo;
  const double sinu = sin(ustereo);
  const double cosu = cos(ustereo);
  const double sinv = sin(vstereo);
  const double cosv = cos(vstereo);
  const double cscuv = 1.0 / sin(ustereo-vstereo);
  const double ufac = sqrt(24.0);
  const double vfac = ufac;
  const double ures = upitch / (cos(0.5*(ustereo-vstereo)) * ufac);
  const double vres = vpitch / (sin(0.5*(ustereo-vstereo)) * vfac);

  htru = new TH2D ("htru", "tru", nbx, xlo, xhi, nby, ylo, yhi);
  hrec = new TH2D ("hrec", "rec", nbx, xlo, xhi, nby, ylo, yhi);
  xres = new TH1D ("xres", "xres", nb, -5*ures, 5*ures);
  yres = new TH1D ("yres", "yres", nb, -5*vres, 5*vres);
  xpul = new TH1D ("xpul", "xpull", nb, -5, 5);
  ypul = new TH1D ("ypul", "ypull", nb, -5, 5);

  for (int ihit=0; ihit<nhit; ihit++) {
    double x = gRandom->Uniform(xwid);
    double y = gRandom->Uniform(ywid);
    double xtru = xlo + x;
    double ytru = ylo + y;
    htru->Fill (xtru, ytru);
    double utru = x*cosu - y*sinu;
    double vtru = x*cosv - y*sinv;
    double urec = round(utru/upitch) * upitch;
    double vrec = round(vtru/vpitch) * vpitch;
    double xrec = xlo + (vrec*sinu - urec*sinv) * cscuv;
    double yrec = ylo + (vrec*cosu - urec*cosv) * cscuv;
    hrec->Fill (xrec, yrec);
    xres->Fill (xrec-xtru);
    yres->Fill (yrec-ytru);
    double xp = (xrec-xtru)/ures;
    double yp = (yrec-ytru)/vres;
    xpul->Fill (xp);
    ypul->Fill (yp);
  }
}



void digi_pixel (
                 int nhit=10000000,
                 double xpitch=0.050,  // 50um pitch
                 double ypitch=0.050,  // 50um pitch
                 double xlo=0.0,
                 double xhi=100.0,
                 double ylo=0.0,
                 double yhi=100.0,
                 int nbx=10000,
                 int nby=10000,
                 int nb=500
                ) {

  const double xwid = xhi-xlo;
  const double ywid = yhi-ylo;
  const double ufac = sqrt(12.0);
  const double vfac = ufac;
  const double ures = xpitch / ufac;
  const double vres = ypitch / vfac;

  htru = new TH2D ("htru", "tru", nbx, xlo, xhi, nby, ylo, yhi);
  hrec = new TH2D ("hrec", "rec", nbx, xlo, xhi, nby, ylo, yhi);
  xres = new TH1D ("xres", "xres", nb, -5*ures, 5*ures);
  yres = new TH1D ("yres", "yres", nb, -5*vres, 5*vres);
  xpul = new TH1D ("xpul", "xpull", nb, -5, 5);
  ypul = new TH1D ("ypul", "ypull", nb, -5, 5);

  for (int ihit=0; ihit<nhit; ihit++) {
    double x = gRandom->Uniform(xwid);
    double y = gRandom->Uniform(ywid);
    double xtru = xlo + x;
    double ytru = ylo + y;
    htru->Fill (xtru, ytru);
    double xrec = xlo + round(xtru/xpitch) * xpitch;
    double yrec = ylo + round(ytru/ypitch) * ypitch;
    hrec->Fill (xrec, yrec);
    xres->Fill (xrec-xtru);
    yres->Fill (yrec-ytru);
    double xp = (xrec-xtru)/ures;
    double yp = (yrec-ytru)/vres;
    xpul->Fill (xp);
    ypul->Fill (yp);
  }
}
