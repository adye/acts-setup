source /cvmfs/sft.cern.ch/lcg/views/LCG_101/x86_64-centos7-gcc11-opt/setup.sh
git clone https://github.com/acts-project/acts .
cmake -B build -S . -DACTS_BUILD_FATRAS=on -DACTS_BUILD_EXAMPLES_PYTHIA8=on -DACTS_BUILD_EXAMPLES_PYTHON_BINDINGS=on
cmake --build build -j8
git lfs install
git clone https://gitlab.cern.ch/atlas/acts-itk.git
du -sh acts-itk
source build/python/setup.sh
Examples/Scripts/Python/full_chain_itk.py 
