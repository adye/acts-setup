#!/bin/bash
# Find all include directories generated by CMake.
# g++ adds -isystem $CPLUS_INCLUDE_PATH which is set to ${LCG_VIEW}/include
# but don't include that, because that would include ${LCG_VIEW}/include/Acts.

abspath() {
  abspath=$(cd "$(dirname "$1")"; pwd)/$(basename "$1")
  echo "${abspath%/.}"
}

lcg_view=$(dirname $CPLUS_INCLUDE_PATH)   # $lcg_view is not exported
[ -n "$1" ] && BUILD=$(abspath "$1") || BUILD=$(dirname $(abspath "$0"))/build
[ -n "$2" ] && SRC=$(abspath "$2") || SRC=$BUILD/src
PYINC=$(dirname $(dirname $(readlink -e "$CPLUS_INCLUDE_PATH"/python*/Python.h)))
cd "$BUILD"
(
    sed -n 's/^ *CXX_INCLUDES *= *//p' $(find . -name flags.make) \
    | sed -e 's/-I *//g' -e 's/-isystem  *//g' -e 's/  */\n/g' \
    | sed -e "s=^$SRC/=\${workspaceFolder}/=" -e "s=^$BUILD/=\${BUILD_DIR}/=" -e "s=^$lcg_view/=\${LCG_VIEW}/=" -e "s=^$PYINC/=\${LCG_VIEW}/include/=" \
    | sort -u
    dirname $(readlink -e "$CPLUS_INCLUDE_PATH/TROOT.h")
    dirname $(dirname $(readlink -e "$CPLUS_INCLUDE_PATH/boost/version.hpp"))
    dirname $(dirname $(readlink -e "$CPLUS_INCLUDE_PATH/tbb/tbb.h"))
) \
| sed -E 's/^(.*)$/                "\1",/'
