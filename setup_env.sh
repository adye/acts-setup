if [ -f "/opt/lcg_view/setup.sh" ]; then
  # Docker container etc
  . $(readlink -f "/opt/lcg_view/setup.sh")
  export DD4hep_DIR=$(readlink -f "/opt/lcg_view")
else
  . "$(dirname $(readlink -f "$BASH_SOURCE"))/setup_cvmfs_lcg105.sh" > /dev/null
  export PATH="/cvmfs/sft.cern.ch/lcg/releases/clang/14.0.6-14bdb/x86_64-${lcg_os/#el9/centos9}/bin:$PATH"
fi
