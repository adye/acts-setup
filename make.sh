#!/bin/bash

ppd_readlink()
{
  readlink "$@" | sed 's=^/net/home/ppd/=/home/ppd/='
}

osrel=$(uname -r)
hostname=$(hostname)
if [ -z "${osrel%%*-WSL2}" ]; then
  njobs=4
elif [ -z "${hostname##mercury*}" ]; then
  njobs=8
else
  njobs=-
fi

cwd=$(dirname $(ppd_readlink -f "$BASH_SOURCE"))
prog=$(basename "$0")
[ -d "$cwd/tmp" ] && notmp=0 || notmp=1
help=0 rem=0 tidy=0 makeopts="" dobuild="" docmake="" dovenv=1
# DD4HEP settings (disable with -O)
xtraopts=(-DACTS_BUILD_EXAMPLES_DD4HEP=on -DACTS_BUILD_ODD=on -DACTS_BUILD_EXAMPLES_DD4HEP=on -DACTS_BUILD_FATRAS_GEANT4=on -DACTS_BUILD_EXAMPLES_GEANT4=on -DACTS_FORCE_ASSERTIONS=on)
build="build" src="acts" setup_env="$cwd/setup_env.sh"
while getopts :hrtTCOPL:b:s:j:m:n arg; do
  case "$arg" in
     h) help=1;;
     r) rem=1;;
     t) tidy=1 build="build-tidy" makeopts="$makeopts -k" dobuild=":";;
     T) tidy=1 build="build-tidy" makeopts="$makeopts -k" dobuild=":" docmake=":";;
     b) build=$OPTARG;;
     s) src=$OPTARG;;
     j) njobs=$OPTARG;;
     C) dobuild=":";;
     L) setup_env=$OPTARG;;
     m) makeopts="$makeopts $OPTARG";;
     O) xtraopts=();;
     P) dovenv=0;;
     n) notmp=1;;
    \?) echo "$prog: invalid option -$OPTARG" >&2; exit 1;;
  esac
done
shift $(($OPTIND-1))

if [ \( $# -lt 1 -a $rem -eq 0 \) -o $help -ne 0 ]; then
  cat <<EOF
$prog - build ACTS

Sets up LCG/key4hep environment, so don't do any extra setup before running.

USAGE:
  $prog [OPTIONS] BUILD-DIRNAME [CMAKE-OPTIONS]

OPTIONS:
  -h       display this help and exit
  -r       remove current build
  -t       build for clang-tidy
  -T       rerun clang-tidy build
  -b NAME  alternate build name (default="$build")
  -s NAME  alternate source name (default="$src")
  -j NJOB  number of threads to build (default=$njobs)
  -C       just run CMake, don't actually build
  -L S.sh  source this script to setup environment (default=$setup_env)
  -m OPT   make options
  -O       don't include ODD in build
  -P       don't create Python venv
  -n       don't use temp directory for build (default if $cwd/tmp doesn't exist)

EXAMPLE:
  $(basename "$0") build04 -DACTS_BUILD_FATRAS=on
  $(basename "$0") -t build-tidy

AUTHOR: Tim Adye <tim.adye@cern.ch>
EOF
  exit 1
fi
sname="$1"
shift

if [ $notmp -eq 0 ]; then
  if [ -L "$cwd/tmpdir" ]; then
    TMP=$(readlink -f "$cwd/tmpdir")/
  else
    test -z "$TMPDIR" && TMPDIR="/tmp/`id -un`"
    TMP="$TMPDIR/acts"
    ln -nfs "$TMP" "$cwd/tmpdir"
    TMP="$TMP/"
  fi
  [ -z "${TMP%%/tmp/*}" -o -z "${TMP%%/scratch/*}" ] && suf=".$(hostname -s)" || suf=""
else
  TMP=""
  suf=""
fi
name="$sname$suf"

if [ $rem -ne 0 ]; then

  if [ -z "$sname" ] && [ -d "$cwd/$build" ]; then
    sname=$(basename $(readlink -f "$cwd/$build"))
    name="$sname$suf"
    [ "$cwd/$build" -ef "$TMP$sname" ] || sname="" name=""
  fi

  if [ -n "$sname" ] && [ -d "$TMP$sname" ]; then
    ( set -x
      rm -rf "$TMP$sname"
      [ $notmp -eq 0 ] && rm "$cwd/tmp/$name"
      rm "$cwd/$build"
    )
  else
    echo "$(basename "$0"): $build directory not found"
  fi

else

  if [ ! -d "tmp/$name" ]; then
    ( set -x
      mkdir -p "$TMP$sname"
      if [ $notmp -eq 0 ]; then
        mkdir -p "$cwd/tmp"
        ln -nfs "$TMP$sname" "$cwd/tmp/$name"
      fi
      ln -s "$cwd/$src" "$TMP$sname/src"
      ln -s "$setup_env" "$TMP$sname/setup_env.sh"
    )
  fi

  if [ $tidy -eq 0 ]; then
    opts=(-DCMAKE_CXX_STANDARD=20 -DACTS_BUILD_FATRAS=on -DACTS_BUILD_EXAMPLES_PYTHIA8=on -DACTS_BUILD_EXAMPLES_PYTHON_BINDINGS=on -DACTS_ENABLE_LOG_FAILURE_THRESHOLD=on -DACTS_BUILD_PLUGIN_ACTSVG=on -DCMAKE_EXPORT_COMPILE_COMMANDS=on)
  else
    opts=(-DCMAKE_CXX_STANDARD=20 -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-Werror" -DACTS_BUILD_EVERYTHING=on -DACTS_RUN_CLANG_TIDY=on)
  fi

  set -x
  export ACTS_BUILD_DIR="$cwd/$build"
  if [ $notmp -eq 0 ]; then
    ln -nfs "tmp/$name" "$cwd/$build"
  elif [ "$name" != "$build" ]; then
    ln -nfs "$name" "$cwd/$build"
  fi
  cd "$cwd/$build"
  set +x

  echo + \
  . setup_env.sh
  . setup_env.sh
  if [ $dovenv -ne 0 ]; then
    echo + \
    . "$cwd/setup_python.sh"
    . "$cwd/setup_python.sh"
  fi

  # LCG_105 has pytest and black installed, but we still seem to require venv. (Just for black old version?)
  set -x
  if [ $dovenv -ne 0 -a ! -x venv/bin/python ]; then
    python -m venv venv
    pip install black==23.12.1   # version used by format-py from .github/workflows/checks.yml
    # These are already available in LCG_105:
    # pip install -r src/Examples/Python/tests/requirements.txt
    # pip install -r src/CI/clang_tidy/requirements.txt
  fi

  $docmake cmake -B . -S src ${opts[@]} "${xtraopts[@]}" "$@"
  $dobuild cmake --build . -- -j "$njobs" $makeopts
  set +x

  if [ $tidy -ne 0 ]; then
    set -x
    cmake --build . -- -j "$njobs" $makeopts 2>&1 | tee build.log
    mkdir clang-tidy
    src/CI/clang_tidy/parse_clang_tidy.py build.log clang-tidy/clang-tidy.json --exclude "*thirdparty*"
    src/CI/clang_tidy/check_clang_tidy.py --report clang-tidy/clang-tidy.json --config src/CI/clang_tidy/limits.yml
  fi

fi
