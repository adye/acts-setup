# setup appropriate LCG 103 release via cvmfs

if test -e /etc/centos-release && grep 'CentOS Linux release 7' /etc/centos-release > /dev/null; then
  lcg_os=centos7
elif test -e /etc/centos-release && grep 'CentOS Stream release 8' /etc/centos-release > /dev/null; then
  lcg_os=centos8
elif test -e /etc/lsb-release && grep 'Ubuntu 22.04' /etc/lsb-release > /dev/null; then
  lcg_os=ubuntu2204
else
  echo "Unsupported system" 1>&2
  return
fi

lcg_release=LCG_103
lcg_compiler=gcc11-opt   # gcc12 has a problem with boost, giving scary warning on compilation
lcg_platform=x86_64-${lcg_os}-${lcg_compiler}
lcg_view=/cvmfs/sft.cern.ch/lcg/views/${lcg_release}/${lcg_platform}

source ${lcg_view}/setup.sh
# extra variables required to build acts
export DD4hep_DIR=${lcg_view}