if [ -n "$1" ]; then
  local_build="$1"
else
  # Use build symlink next to symlink to setup.sh, which may be different from that next to actual location of setup.sh.
  # This is different from setup_acts.sh default.
  local_build=$(readlink -f $(dirname "$BASH_SOURCE")/build)
  [ -d "$local_build" ] || local_build=""
fi
if [ -n "$local_build" ] && [ -r "$local_build/setup_env.sh" ]; then
  . "$local_build/setup_env.sh"
else
  . "$(dirname $(readlink -f "$BASH_SOURCE"))/setup_env.sh"
fi
. "$(dirname $(readlink -f "$BASH_SOURCE"))/setup_acts.sh" "$local_build"
. "$(dirname $(readlink -f "$BASH_SOURCE"))/setup_python.sh"
unset local_build
