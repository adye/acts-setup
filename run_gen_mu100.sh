#!/bin/bash

run() {
  d=$1
  shift
  mkdir -p "$d"
  rm -f "$d"/*
  (
    set -x
    job -qwo "$d/$1.log" "$@"
  )
}

evt=gen_mu100/evt
sim=gen_mu100/sim
rec=gen_mu100/rec
ttr=gen_mu100/ttr

run "$evt" ActsExampleParticleGun -n100 --gen-nparticles=100 --rnd-seed=42 --gen-mom-gev=0.4:50 --gen-mom-transverse --gen-randomize-charge --output-dir="$evt" --output-csv
run "$sim" ActsExampleFatrasGeneric    --bf-constant-tesla=0:0:2 --input-dir="$evt" --output-dir="$sim" --output-csv
run "$rec" ActsExampleCKFTracksGeneric --bf-constant-tesla=0:0:2 --digi-config-file=acts/acts/Examples/Algorithms/Digitization/share/default-smearing-config-generic.json --geo-selection-config-file=acts/acts/Examples/Algorithms/TrackFinding/share/geoSelection-genericDetector.json --input-dir="$sim" --output-dir="$rec"
run "$ttr" ActsExampleCKFTracksGeneric --bf-constant-tesla=0:0:2 --digi-config-file=acts/acts/Examples/Algorithms/Digitization/share/default-smearing-config-generic.json --ckf-truth-smeared-seeds --input-dir="$sim" --output-dir="$ttr"
