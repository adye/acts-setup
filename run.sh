#!/bin/bash

stdsteps=evt,sim,rec

prog=$(basename "$0")
help=0 test="" job=0 int=0 jopt="" eopt="" opts=()
while getopts :htjiJ:e: arg; do
  case "$arg" in
     h) help=1;;
     t) opts+=(-$arg) test=":";;
     j) job=1;;
     J) opts+=(-$arg "$OPTARG") jopt=-j$OPTARG;;
     e) opts+=(-$arg "$OPTARG") eopt=$OPTARG;;
     i) opts+=(-$arg) int=1;;
    \?) echo "$prog: invalid option -$OPTARG" >&2; exit 1;;
  esac
done
shift $(($OPTIND-1))

if [ $# -lt 1 -o $help -ne 0 ]; then
  cat <<EOF
$prog - run ACTS full-chain for ITk example

USAGE:
  $prog [-htji] SAMPLE [STEPS [OPTIONS]]

STEPS $stdsteps can be specified with a suffix to write to a new directory

OPTIONS:
  -h   display this help and exit
  -t   test - just show the commands the would be run
  -j   submit whole thing as a subprocess using the 'job' command
  -i   don't submit each step as a job, but just run interactively
  -e O add evt options
  -J N run N parallel threads

AUTHOR: Tim Adye <tim.adye@cern.ch>
EOF
  exit 1
fi

if [ $job -ne 0 ]; then
  mkdir -p "$1"
  job +o "$1/$prog.log" "$0" "${opts[@]}" "$@"
  exit
fi

sample=$1
[ -n "$2" ] && steps=,$2, || steps=,$stdsteps,
shift; shift
last_step=""

run() {
  step=$1
  shift
  [ -z "$last_step" ] && input=() || input=(--input-dir="$sample/$last_step")
  last_step="$step"
  re_step=',('"$step"'[^,]*),'
  [[ "$steps" =~ $re_step ]] || return
  subdir=${BASH_REMATCH[1]}
  [ -z "$subdir" ] && return   # just in case
  if [ $# -lt 1 ]; then
    echo "$prog: undefined $step sample '$sample'" >&2
    exit 2
  fi
  last_step="$subdir"
  d=$sample/$subdir
  if [ -z "$test" ]; then
    mkdir -p "$d"
    rm -f "$d"/*
  fi
  if [ $int -ne 0 ]; then
    jobcmd=()
  else
    jobcmd=(job -qwo "$d/$1.log")
  fi
  (
    set -x
    $test "${jobcmd[@]}" exec "$@" "${input[@]}" --output-dir="$d"
  )
}

evt_extra=($jopt $eopt --rnd-seed=42 --output-csv)
sim_extra=($jopt)
rec_extra=($jopt)

re_mu='^mu([0-9]+)(k?)$'
re_ttbar='^ttbar_pu([0-9]+)$'
re_seed='(--ckf-[^ ]+-seeds|--geo-selection-config-file)'
if   [ "$sample" = "1mu" ]; then
  run evt ActsExampleParticleGun \
    -n1                          \
    --gen-mom-gev=5:5            \
    --gen-eta=0.5:0.5            \
    --gen-phi-degree=45:45       \
    --gen-mom-transverse         \
    "${evt_extra[@]}" "$@"
elif [[ "$sample" =~ $re_mu ]]; then
  n=${BASH_REMATCH[1]}
  [ "${BASH_REMATCH[2]}" = "k" ] && let n=1000*n
  run evt ActsExampleParticleGun \
    -n$n                         \
    --gen-nparticles=100         \
    --gen-mom-gev=0.4:50         \
    --gen-mom-transverse         \
    --gen-randomize-charge       \
    "${evt_extra[@]}" "$@"
elif [[ "$sample" =~ $re_ttbar ]]; then
  pu=${BASH_REMATCH[1]}
  [ "$pu" = "200" ] || evt_extra+=(--gen-npileup="${BASH_REMATCH[1]}")
  run evt ActsExamplePythia8                \
    -n100                                   \
    --gen-hard-process="Top:qqbar2ttbar=on" \
    "${evt_extra[@]}" "$@"
  sim_extra+=(--select-eta=-4:4 --select-pt-gev=0.4: --remove-neutral)
else
  run evt # error
fi

simrec_extra=( \
  --geo-tgeo-jsonconfig=acts-itk/itk-hgtd/tgeo-atlas-itk-hgtd.json \
  --geo-tgeo-filename=acts-itk/itk-hgtd/ATLAS-ITk-HGTD.tgeo.root   \
  --mat-input-type=file                                         \
  --mat-input-file=acts-itk/itk-hgtd/material-maps-ITk-HGTD.json   \
  --bf-map-file=acts-itk/bfield/ATLAS-BField-xyz.root              \
)
[[ "$*" =~ $re_seed ]] || rec_extra+=(--ckf-truth-smeared-seeds)

run sim ActsExampleFatrasTGeo                                   \
    --rnd-seed=43                                               \
    "${simrec_extra[@]}"                                        \
    --output-csv                                                \
    "${sim_extra[@]}" "$@"

run rec ActsExampleCKFTracksTGeo                                \
    --rnd-seed=44                                               \
    "${simrec_extra[@]}"                                        \
    --digi-config-file=acts-itk/itk-hgtd/itk-smearing-config-no-hgtd.json \
    "${rec_extra[@]}" "$@"
